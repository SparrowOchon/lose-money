# Lose Money
The following is a Roulette script. For Duelbits using The Martingale System. During testing increased bitcoin balance by 10x. 

**NOTE: The Odds are still at best 48.6% with this strategy. So don't gamble unless you are fine with losing said money.** 

## Features:
- Auto Login (Avoids Recaptcha)
- Auto join Live Roulette
- Automatically perform the Martingale strategy
- Identify when the Balance is Zero and leave. 
- Easily Modifiable to different strategies. I.e (Romanosky, Paroli, Labouchere ... etc)

## Installation
1. The following uses `conda` to manage dependencies which can be installed using: `conda create --name <env> --file requirements.txt`
2. Add your duelbits email and password into the `lose_money` script.

## License
[Share don't Sell License](https://gitlab.com/SparrowOchon/show-dont-sell)
